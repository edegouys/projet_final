resource "azurerm_resource_group" "rg" {
    name= "${var.name_rg}"
    location = "${var.location}"

    tags {
        owner= "${var.owner}"
    }
}
resource "azurerm_virtual_network" "vNet" {
    name= "${var.name_vnet}"
    address_space= ["${var.address_space}"]
    location= "${azurerm_resource_group.rg.location}"
    resource_group_name= "${azurerm_resource_group.rg.name}"  
}

resource "azurerm_subnet" "subNetTest" {
    name= "${var.name_subNet}_Test"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "${var.address_prefix_test}"
}

resource "azurerm_subnet" "subNetProd" {
    name= "${var.name_subNet}_Prod"
    resource_group_name= "${azurerm_resource_group.rg.name}" 
    virtual_network_name= "${azurerm_virtual_network.vNet.name}"
    address_prefix= "${var.address_prefix_prod}"
}

resource "azurerm_network_security_group" "NSecure" {
  name= "${var.name_secure}"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  

    security_rule {
        name= "SSH"
        priority= "1001"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "22"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }
    security_rule {
        name= "HTTP"
        priority= "1002"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "80"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    } 
    security_rule {
        name= "App"
        priority= "1003"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "8080"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }  
    security_rule {
        name= "DB"
        priority= "1004"
        direction= "Inbound"
        access= "Allow"
        protocol= "Tcp"
        source_port_range= "*"
        destination_port_range= "27017"
        source_address_prefix= "*"
        destination_address_prefix= "*"
    }   
} 


resource "azurerm_public_ip" "IPTest" {
  name= "${var.name_IP}_Test"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  allocation_method= "Static"
  domain_name_label= "${var.name_DNS}-test"
}

resource "azurerm_public_ip" "IPProd" {
  name= "${var.name_IP}_Prod"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  allocation_method= "Static"
  domain_name_label= "${var.name_DNS}-prod"
}

resource "azurerm_network_interface" "NICJTest" {
  name= "${var.name_ni}_JTest"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_JTest"
    subnet_id= "${azurerm_subnet.subNetTest.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.1.67"
    public_ip_address_id= "${azurerm_public_ip.IPTest.id}"
  }
}

resource "azurerm_network_interface" "NICBDTest" {
  name= "${var.name_ni}_BDTest"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_BDTest"
    subnet_id= "${azurerm_subnet.subNetTest.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.1.66"
  }
}
resource "azurerm_network_interface" "NICJProd" {
  name= "${var.name_ni}_JProd"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_JProd"
    subnet_id= "${azurerm_subnet.subNetProd.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.2.65"
    public_ip_address_id= "${azurerm_public_ip.IPProd.id}"
  }
}

resource "azurerm_network_interface" "NICBDProd" {
  name= "${var.name_ni}_BDProd"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name = "${azurerm_resource_group.rg.name}" 
  network_security_group_id= "${azurerm_network_security_group.NSecure.id}"

  ip_configuration {
    name= "${var.ip_config}_BDProd"
    subnet_id= "${azurerm_subnet.subNetProd.id}"
    private_ip_address_allocation = "Static"
    private_ip_address= "10.0.2.64"
  }
}
resource "azurerm_virtual_machine" "ServProd" {
  name= "${var.name_vm_java}_prod"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICJProd.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_ServProd"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "ServProd"
    admin_username= "ServProd"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/ServProd/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}

resource "azurerm_virtual_machine" "ServTest" {
  name= "${var.name_vm_java}_Test"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICJTest.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_ServTest"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "ServTest"
    admin_username= "ServTest"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/ServTest/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}

resource "azurerm_virtual_machine" "BDProd" {
  name= "${var.name_vm_DB}_prod"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICBDProd.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_BDProd"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "BDProd"
    admin_username= "BDProd"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/BDProd/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}

resource "azurerm_virtual_machine" "BDTest" {
  name= "${var.name_vm_DB}_Test"
  location= "${azurerm_resource_group.rg.location}"
  resource_group_name= "${azurerm_resource_group.rg.name}"  
  network_interface_ids= [ "${azurerm_network_interface.NICBDTest.id}" ]
  vm_size= "${var.vmSize}"

  storage_image_reference {
    publisher= "OpenLogic"
    offer= "CentOS"
    sku= "7.5"
    version= "latest"
  }

  storage_os_disk {
    name= "osdisk_BDTest"
    caching= "ReadWrite"
    create_option= "FromImage"
    managed_disk_type= "Standard_LRS"
  }

  os_profile {
    computer_name= "BDTest"
    admin_username= "BDTest"
  }

  os_profile_linux_config {
      disable_password_authentication= true

      ssh_keys {
          path= "/home/BDTest/.ssh/authorized_keys"
          key_data= "${var.key_data}"
      }
  }
}